# Download RFI's

This script interfaces with Procore's API.

It gets a list of all RFI's names from Procore (Procore does not have a way, as of writing this, to download all RFI attachments).

Uses a manually download document containing a list of RFI's including links to their attachments. 

The Script reads that pdf, highlights all locations of RFI links and downloads them with their corresponding RFI # included in front of their name.

